import { render, screen } from '@testing-library/react';
import App from './App';

const linterror = 123;

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
